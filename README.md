# Описание

Простая настройка бэкапов на северах.

Ключевые моменты:
- Настройка через yml файл
- Для запуска используется одна точка входа
 	В cron можно указать один файл, а все рассписание будет в файлах настроек

## CHANGE LOG

- 1.3
 	- Исправление ошибок
- 1.2
 	- Вывод версии
 	- Dropbox API v2
 	- Mini bugfix
- 1.1
 	- Исправление ошибок в блоках place.mysql, place.base

# Разработка

## Локализация

```
cd ./
pygettext -a -d scb source/
```