#!/usr/bin/env python3

from docopt import docopt
import yaml
import sys
import os

from lib import main
from lib.exception import ScbException
from lib.i18n import _
from lib import __version__

usage_msg = _('''Simple CLI Backup
Usage:
	scb.py --config=<config_file>
	scb.py init --config=<config_file>
	scb.py --config-test=<config_file>
	scb.py (-h | --help)
	scb.py (-v | --version)
	scb.py --plugin-info

Options:
	-h --help           Show this message
	-v --version        Show version
	--config            The path to the file with the settings
	--config-test       Testing config file
	init                Initializing modules, if necessary

	--plugin-info      Displays information about plugins
''')

def read_file_config(config_file):
	''' Читает файл настроек

	Returns
		Содержимое файла в json
	'''
	if not os.path.isfile(config_file):
		print(_('The file of settings not found'))
		sys.exit(-1)

	with open(config_file, "r") as f:
		dt = yaml.load(f, Loader=yaml.FullLoader)

	return dt

def plugin_info():
	''' Отображает информацию об плагинах (target и place)
	'''
	import lib.main
	from lib.base import TargetBase, PlaceBase

	print(_("Target plugins:"))
	for cls in TargetBase.__subclasses__():
		print(_("    {} - {}").format(cls.name, cls.description))
	print("")
	print(_("Place plugins:"))
	for cls in PlaceBase.__subclasses__():
		print(_("    {} - {}").format(cls.name, cls.description))

	sys.exit(0)

if __name__ == '__main__':
	# arguments = docopt(_(__doc__))
	arguments = docopt(usage_msg)

	# Версия
	if arguments['-v'] or arguments['--version']:
		print(__version__)
		sys.exit(0)

	# Информация
	if arguments['--plugin-info']:
		plugin_info()

	# Инициализация модулей, режим
	mode_init = arguments['init']

	# Режим проверки файла настроек
	mode_config_test = False
	if arguments["--config-test"] is not None:
		mode_config_test = True
		config_file = arguments["--config-test"]

	if arguments["--config"] is not None:
		config_file = arguments['--config']

	dt = read_file_config(config_file)

	if mode_config_test:
		try:
			srv = main.scb(config= dt)
		except ScbException as e:
			print( _("Error: \n\t{}").format(e))
			sys.exit(-1)
		else:
			print("Success")
		sys.exit(0)

	# Запуск
	try:
		srv = main.scb(config= dt)
		if mode_init:
			srv.init()
		else:
			srv.process()
	except KeyboardInterrupt:
		print(_("\nThe operation was interrupted"))
		sys.exit(0)
	except ScbException as e:
		print("[E]", e)
		sys.exit(-1)

