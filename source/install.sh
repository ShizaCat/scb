#!/bin/bash

# ============================================
# Define
# ============================================

WORK_DIR=/opt/scb
PATH_SCRIPT=/usr/bin/scb

BODY_SCRIPT="#!/bin/bash
. ${WORK_DIR}/bin/activate
${WORK_DIR}/source/scb.py \$@
deactivate
"

# ============================================
# Functions
# ============================================

function deb_install {
	if [[ $EUID -ne 0 ]] ; then
		echo 'Please run as root'
		exit 1
	fi

	if [[ -d ${WORK_DIR} ]] ; then
		echo "Directory ${WORK_DIR} exist"
		exit 1
	fi

	apt-get update

	apt-get install -y python3 python3-pip

	apt-get install -y python3.4-venv

	mkdir -p ${WORK_DIR}/source

	python3 -m venv ${WORK_DIR}

	. /opt/scb/bin/activate

	pip3 install docopt
	pip3 install pyyaml
	pip3 install jsonschema
	pip3 install dropbox

	# Mysql Connector
	(
	wget https://github.com/mysql/mysql-connector-python/archive/2.1.7.tar.gz -O /tmp/2.1.7.tar.gz
	tar -xzf /tmp/2.1.7.tar.gz -C /tmp
	cd /tmp/mysql-connector-python-2.1.7/
	python3 setup.py install
	cd /
	rm -rf /tmp/mysql-connector-python-2.1.7 /tmp/2.1.7.tar.gz
	)

	deactivate

	cp -r ./lib ${WORK_DIR}/source
	cp scb.py ${WORK_DIR}/source

	echo "${BODY_SCRIPT}" > ${PATH_SCRIPT}
	chmod +x ${PATH_SCRIPT}
}

function rhel_install {
	echo 'rhel'
}

# ============================================
# Main
# ============================================

IS_DEB=0
IS_RHEL=0

RUN=0

#
# What OS are we on
#

apt-get -h > /dev/null 2>&1
if (($? == 0)) ; then IS_DEB=1 ; fi

yum --help > /dev/null 2>&1
if (($? == 0)) ; then IS_RHEL=1 ; fi

#
# Run an OS-specific installer
#

if [ $IS_DEB -eq 1 ] ; then deb_install ; RUN=1 ; fi

if [ $IS_RHEL -eq 1 ] ; then rhel_install ; RUN=1 ; fi

#
# Unknown system
#

if [ $RUN -ne 1 ]
then
   echo "Could not find apt-get nor yum. OS could not be determined, installer cannot run."
   exit 1
fi