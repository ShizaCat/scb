
import tempfile
import jsonschema
import tarfile
import os
import traceback
from datetime import datetime
from functools import reduce

from lib.i18n import _
from lib.exception import ScbException, ScbConfigError
from lib.schema import schema_base_format_config

from lib.base import TargetBase, PlaceBase
# Places
from lib.place.folder import folder
from lib.place.dropbox import dropbox as drbox
# Target
from lib.target.file import file
from lib.target.mysql import mysql_target
# Notify
from lib.notification import notification, status_notify
from lib.notify.base import NotifyBase
from lib.notify.email import EmailNotify

class scb:
	
	settings = {
		'tmp_base': '/tmp',			# База для временных директорий
	}

	def __init__(self, config: dict):
		'''
			Args:
				config: содержит преобразованный в словарь файл настроек
		'''
		self._config = config

		# Проверка конфигурации
		self.check_config()

	def check_config(self, silence=False):
		'''
		Проверка конфигурации
		Выводит сообщения об ошибке при таковой
		'''
		self._check_config_main_sections()

		try:
			# Проверяем, что все типы для place зарегистрированы
			for place_config in self._config['places']:
				place_obj = PlaceBase.factory(place_config)
				place_obj.check_field_config()

			# Проверяем, что все типы для target зарегистрированы
			for item in [ slise["target"] for slise in self._config["slises"] ]:
				for target_config in item:
					target_obj = TargetBase.factory(target_config)

		except ScbException as e:
			raise ScbConfigError(str(e))

		# Проверяем, что все place в target существуют
		for item in [ slise["place"] for slise in self._config["slises"] ]:
			for place_name_in_target in item:
				if place_name_in_target not in [ place["name"] for place in self._config["places"]]:
					raise ScbConfigError( _("For target not found place: '{place}'").format(place=place_name_in_target) )

		# Ппроверяем, что все notify в slise сущеуствуют
		for item in [ slise["notify"] for slise in  self._config["slises"] if "notify" in self._config["slises"] ]:
			for notify_name in item:
				if notify_name not in [ notice["name"] for notice in self._config["notices"]]:
					raise ScbConfigError( _("For notify not found notice: '{notice}'").format(notice=notify_name) )

	def _check_config_main_sections(self):
		'''
			Проверка на валидность основных секций


			Raises:
				ScbConfigError - в случаии наличия ошибки в формате,
					с описанием самой ошибки
		'''
		try:
			jsonschema.validate( self._config, schema_base_format_config)
		except jsonschema.exceptions.ValidationError as e:
			raise ScbConfigError( _("Configuration errors: %s") % str(e.message))

	def process(self):
		'''
			Основная задача обработки файла конфигурации
		'''	
		self._process_slises()

	def init(self):
		'''
			Режим инициализации модулей, если это необходимо
		'''
		for place_config in self._config['places']:
			PlaceBase.factory(place_config).init()
	
	def _process_slises(self):
		'''
			Последоваетельный запуск процессов для всех срезов
		'''

		for slise in self._config['slises']:

			list_files = []

			# Временная директория
			tmp_dir = tempfile.TemporaryDirectory(dir= self.settings['tmp_base'])
			# Директория для результирующего файла
			tmp_dir_result = tempfile.TemporaryDirectory(dir= self.settings['tmp_base'])
			# Нотификации
			notify_msg = notification(slise_name= slise['name'])

			for target in slise['target']:
				target_obj = TargetBase.factory(target)
				
				try:
					result_filename = target_obj.process(tmp_dir.name, tmp_dir_result.name)
				except:
					tr = traceback.format_exc()
					notify_msg.add_target(name= target['name'], status = status_notify.bad, status_description= tr)
				else:
					list_files.append( os.path.join( tmp_dir_result.name, result_filename))
					notify_msg.add_target(name= target['name'], status = status_notify.good)

			if len(list_files) > 0:
				# Все цели из среза в один файл
				slise_result_file = self._slise_create_result_file(list_files, tmp_dir_result.name, slise['name'])

				# Запускаем place's для полученной цели
				for place_name in slise['place']:
					self._place_start(slise_result_file, place_name, notify_msg)
			else:
				# Ошибка должны быть цели
				pass

			# Уведомления
			if "notify" in slise:
				for notify_item in slise["notify"]:
					self._notify_start(
						notice_name = notify_item['name'],
						body = notify_msg.get_body(),
						status = notify_item['status'] if 'status' in notify_item else None
					)

	def _slise_create_result_file(self, list_files: list, tmp_dir: str, slise_name: str) -> str:
		'''
			Создание итогового файла среза
			Собирается архив их всех целей


			Args:
				list_files - список файлов для архива, полные пути
				tmp_dir - директория куда положить итоговый файл
				slise_name - имя среза

			Return:
				Полный путь итогового файла
		'''
		target_file = os.path.join(tmp_dir, self._generate_name_slise(slise_name))

		with tarfile.open(target_file, "w") as tar:
			for result_file in list_files:
				tar.add( result_file, arcname= os.path.split(result_file)[1])

		return target_file

	def _generate_name_slise(self, slise_name: str) -> str:
		'''
			Генерируем имя для файла среза


			Args:
				slise_name - имя среза


			Returns:
				Взвращает имя файла среза
		'''

		dt = datetime.today()

		ext_file = "tar"

		return "%s_%s.%02i.%02i.%s" % (slise_name, dt.year, dt.month, dt.day, ext_file)

	def _place_start(self, slise_result_file: str, place_name: str, notify_msg):
		'''
			Запуск места для среза
			Ложит итоговой файл(ы) в указанное место


			Args:
				slise_result_file - полный путь к файлу для отправки
				place_name - имя места
				notify_msg - инстанс класса notification
		'''
		for place_config in self._config['places']:
			if not place_config['name'] == place_name:
				continue
			
			place_obj = PlaceBase.factory(place_config)
			
			try:
				place_obj.process(slise_result_file)
			except:
				tr = traceback.format_exc()
				notify_msg.add_place(name= place_name, status= status_notify.bad, status_description= tr)
			else:
				notify_msg.add_place(name= place_name, status= status_notify.good)
			
			return

		msg_error = _("'Place' %s not found") % (place_name,)
		notify_msg.add_place(name= place_name, status= status_notify.bad, status_description= msg_error)

	def _notify_start(self, notice_name:str, body:str, status):
		'''
		Запускает класс нотификаций


		Args:
			notice_name - название для настроек нотификации
			body - тело сообщения
			status - Перечисление типа status_notify
		'''
		for notice in self._config['notices']:
			if notice['name'] == notice_name:
				obj = NotifyBase.factory(notice)
				obj.send(body, status)
				return

		raise ScbException( "Не найден notice: %s" % notice_name)

