from lib.base import TargetBase
from lib.exception import ScbException
from lib.i18n import _

import os
import re
import tarfile
from datetime import datetime

import jsonschema

import mysql.connector
from mysql.connector import MySQLConnection

class mysql_dump:
	# Max siza packet (16kb)
	max_allowed_packet = 16 * 1024

	name = "Python MySQL Dump"

	def __init__(self, host, user, password, database, port= None, charset= None):


		self._host = host
		self._port = port
		self._user = user
		self._password = str(password)
		self._database = database
		self._charset = charset

	def dump(self, filename:str):
		'''Дамп базы в указанный файл

		Args:
			filename - имя файла, в который будет сохранен дамп
		'''
		try:
			args = {
				"user" : self._user, 
				"password" : self._password, 
				"host" : self._host, 
				"database" : self._database,
			}
			if not self._port is None:
				args['port'] = self._port
			if not self._charset is None:
				args['charset'] = self._charset

			self.conn = MySQLConnection( **args)
			self.cur = self.conn.cursor()

			self._dump(filename, self.cur)

			self.cur.close()
			self.conn.close()
		except mysql.connector.Error as e:
			raise ScbException(str(e))

	def _dump_create_header(self, file, cur):
		file.write('-- {name}\n'.format(name=self.name).encode())
		file.write(b'--\n')
		file.write('-- Host: {host}    Database: {db}\n'.format(host=self._host, db=self._database).encode())
		file.write(b'-- ------------------------------------------------------\n')

		cur.execute('SELECT VERSION();')
		v = cur.fetchone()[0]
		file.write('-- Server version {}\n'.format(v).encode())

		file.write(b'\n')

		file.write(b'''/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
\n''')

	def _dump_create_bottom(self, file, cur):
		file.write(b'''/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
''')
		file.write("\n-- Dump completed on {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")).encode())

	def _dump(self, filename, cur):
		with open(filename, 'wb') as f:
			self._dump_create_header(f, cur)

			tables = self._get_tables(cur)

			for table in tables:
				f.write( '--\n-- Table structure for table `{table}`\n--\n\n'.format(table= table).encode() )
				f.write( 'DROP TABLE IF EXISTS `{table}`;\n\n'.format(table= table).encode() )
				f.write( self._get_create_table(cur, table).encode() )

				f.write( '--\n-- Dumping data for table `{table}`\n--\n\n'.format(table= table).encode() )

				cur.execute( 'SELECT * FROM `{table}`;'.format(table= table) )

				base_stmt = 'INSERT INTO `{table}` VALUES '.format(table= table).encode()
				row_stmt_buf = b''
				first = True
				for row in cur.fetchall():
					row_stmt = self._get_row_to_str(row)
					if len(row_stmt) + len(base_stmt) + len(row_stmt_buf) >= self.max_allowed_packet:
						if first:
							f.write( 'LOCK TABLES `{table}` WRITE;\n'.format(table= table).encode() )
							first = False
						f.write( base_stmt)
						f.write( row_stmt_buf)
						f.write( b';\n')
						row_stmt_buf = row_stmt
					else:
						if row_stmt_buf:
							row_stmt_buf += b', ' + row_stmt
						else:
							row_stmt_buf = row_stmt
				
				if row_stmt_buf:
					f.write( base_stmt)
					f.write( row_stmt_buf)
					f.write( b';\n')

				if not first:
					f.write( 'UNLOCK TABLES;\n\n'.encode())

			self._dump_create_bottom(f, cur)

	def _get_tables(self, cur):
		cur.execute('SHOW TABLES')
		return [ x[0] for x in cur.fetchall()]

	def _get_create_table(self, cur, table):
		cur.execute('SHOW CREATE TABLE `{table}`;'.format(table= table))
		return "{sh_table};\n\n".format(sh_table= str(cur.fetchone()[1]))

	def _get_row_to_str(self, row):
		'''
		return
			('','','')
		'''
		if len(row) == 0:
			return ''

		data = '('
		params = {}
		for index in range(len(row)):
			data += '%({})s, '.format(index)
			params[str(index)] = row[index]
		
		data = data[:-2]
		data += ')'

		stmt = mysql.connector.cursor._bytestr_format_dict(
                    data.encode('utf8'), self.cur._process_params_dict(params))
		
		stmt = stmt.replace(b'\0', b'\\0')

		return stmt


class mysql_target(TargetBase):
	'''
		Цель - бэкапит данные из mysql базы данных
		Пример куска конфигурации
		host
		database
		  - <name>
		user
		passwrord
	'''
	jsonschema = {
		"type": "object",
		"properties": {
			"host": {
				"type": "string",
				"description": "Адрес mysql сервера"
			},
			"port": {
				"type": "string",
				"description": "Порта mysql сервера, по умолчанию 3306"
			},
			"database": {
				"type": "array",
				"items": {
					"type": "string"
				},
				"description": "Список баз данных"
			},
			"user": {
				"type": "string",
				"description": "Имя пользователя"
			},
			"password": {
				"type": "string",
				"description": "Пароль"
			},
			"charset": {
				"type": "string",
				"description": "Кодировка для базы данных"
			}
		},
		"required": [ "host", "database", "user", "password" ]
	}

	name = "mysql"
	description = _("Makes mysql database dump")

	def __init__(self, config):
		super().__init__(config)

	def process(self, tmp_dir, dir_result):

		list_files_result = []

		for database in self._config['database']:
			mdump = mysql_dump(
				host = self._config['host'],
				user = self._config['user'],
				password = self._config['password'],
				database = database,
				port = self._config.get('port', None),
				charset= self._config.get('charset', None)
			)

			path_full = os.path.join( tmp_dir, self._generate_name_database(database))

			list_files_result.append( path_full)

			mdump.dump( path_full)

		target_file = os.path.join(dir_result, self._generate_name())

		with tarfile.open(target_file, "w:gz") as tar:
			for path in list_files_result:
				if os.path.exists(path):
					tar.add(path, arcname= os.path.split(path)[1])

		return target_file


	def check_field_config(self):
		super().check_field_config()

		try:
			jsonschema.validate( self._config, self.jsonschema)
		except jsonschema.exceptions.ValidationError as e:
			raise ScbException( _("Configuration error 'target': %s") % e.message)

	def _generate_name(self) -> str:
		'''
			Формирует имя для итогового файла


			Return:
			имя для файла
		'''
		dt = datetime.today()

		ext_file = "tar.gz"

		return "%s_%s.%02i.%02i.%s" % (self._config['name'], dt.year, dt.month, dt.day, ext_file)

	def _generate_name_database(self, database: str) -> str:
		'''
			Формирует имя для файла базы данных

			
			Args:
				database - имя базы данных


			Return:
			имя для файла
		'''
		dt = datetime.today()

		ext_file = "sql"

		return "%s_%s.%02i.%02i.%s" % (database, dt.year, dt.month, dt.day, ext_file)