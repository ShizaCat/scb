

from lib.base import TargetBase
from lib.exception import ScbException
from lib.i18n import _

import os
import tarfile
import jsonschema
from datetime import datetime

class file(TargetBase):
	'''
		Цель - файлы и директории
	'''

	jsonschema = {
		"type": "object",
		"properties": {
			"path": {
				"type": "array",
				"items": {
					"type": "string"
				},
				"description": "Путь к каталогу, в которм лежат файлы для цели"
			}
		},
		"required": [ "path" ]
	}

	name = "file"
	description = _("Working with file and folder")
	
	def __init__(self, config):
		super().__init__(config)

		self.check_field_config()

	def check_field_config(self):
		super().check_field_config()

		try:
			jsonschema.validate( self._config, self.jsonschema)
		except jsonschema.exceptions.ValidationError as e:
			raise ScbException( _("Configuration error 'target': %s") % e.message)

	def process(self, tmp_dir, dir_result):
		target_file = os.path.join(dir_result, self._generate_name())

		with tarfile.open(target_file, "w:gz") as tar:
			for path in self._config['path']:
				if os.path.exists(path):
					tar.add(path) #arcname=os.path.basename(path)

		return target_file

	def _generate_name(self) -> str:
		'''
			Формирует имя для итогового файла


			Return:
			имя для файла
		'''
		dt = datetime.today()

		ext_file = "tar.gz"

		return "%s_%s.%02i.%02i.%s" % (self._config['name'], dt.year, dt.month, dt.day, ext_file)