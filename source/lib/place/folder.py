
from lib.base import PlaceBase
from lib.exception import ScbException
from lib.i18n import _

import os
import jsonschema
import shutil
import random
import re
			
		# except Exception as e:
		# 	# !!!
		# 	print('Исключение в process target')

class folder(PlaceBase):
	'''
		Ложит файлы в указанную папку
	'''

	name = 'folder'
	description = _("Files will be copied to the folder")

	# схема для настроек
	jsonschema_folder = {
		"type": "object",
		"properties": {
			"path": {"type": "string"},
		},
		"required": ["path"]
	}

	def __init__(self, config):
		super().__init__(config)

	def _process(self, result_file):
		try:
			# куда будем копировать
			to_path = self._config['path']

			tmp1, file_name = os.path.split(result_file)
			new_file_name = self._need_add_random(file_name)
			to_path = os.path.join(to_path, new_file_name)

			# Копируем
			shutil.copy2(result_file, to_path)
		except OSError as e:
			raise ScbException(_("Error copying: %s") % str(e))

	def _check_field_config(self):
		jsonschema.validate( self._config, self.jsonschema_folder)

		if not os.path.isdir(self._config['path']):
			raise ScbException(_("The path %s not exists or he is not folder") % self._config['path'])

	def _get_list_file_in_place(self):
		path = self._config['path']

		result_list = []

		for item in os.listdir(path):
			if not os.path.isfile( os.path.join(path, item)):
				continue

			if re.match( self.pattern_file_name, item) is None:
				continue

			result_list.append(item)

		return result_list

	def _delete_file(self, file_name):
		os.remove( os.path.join( self._config['path'], file_name))



