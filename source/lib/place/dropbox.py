import os
import re
import shutil
import random

import requests
import jsonschema
import dropbox as dropbox_main

from lib.base import PlaceBase
from lib.exception import ScbException, ScbErrorSlise
from lib.i18n import _


class dropbox(PlaceBase):
	'''
		Ложит файлы в dropbox
	'''

	name = 'dropbox'
	description = _("Files will be copied to the dropbox")

	# схема для настроек
	jsonschema_dropbox = {
		"type": "object",
		"propertyes": {
			"token": {"type": "string"},
		},
		"required": ["token"]
	}

	# Настройки для dropbox
	_app_key = "2n6bmuohpoijgvn"
	_app_secret = "70mys7n9um4vlce"

	def __init__(self, config):
		super().__init__(config)
		
		self.check_field_config()

	def _process(self, result_file):	
		tmp1, file_name = os.path.split(result_file)
		rel_path = os.path.join( self._get_name_folder_for_save(), file_name)

		try:
			dbx = dropbox_main.Dropbox(self._config['token'])

			# Проверяем существует ли папки, и если надо создаем
			if not self._exist_folder_path('', self._get_name_folder_for_save()):
				dbx.files_create_folder( self._get_name_folder_for_save())

			new_file_name = self._need_add_random(file_name)
			rel_path = os.path.join( self._get_name_folder_for_save(), new_file_name)

			f = open(result_file, 'rb')
			response = self._upload_chunks(result_file, dbx, f, rel_path)
			# response = dbx.files_upload(f.read(), rel_path)
		except dropbox_main.exceptions.DropboxException as e:
			raise ScbErrorSlise(_("Error in 'Dropbox': %s") % e)
		except requests.exceptions.RequestException as e:
			raise ScbErrorSlise(_("Error in 'Dropbox': {}").format(e))

	def _check_field_config(self):
		jsonschema.validate(self._config, self.jsonschema_dropbox)

	def init(self):
		if self._config['token'] != '' and not self._config['token'] is None:
			return

		print(_("Initializing for: %s") % self._config['name'])
		print(_("Perform the following steps:"))

		auth_flow = dropbox_main.DropboxOAuth2FlowNoRedirect(self._app_key, self._app_secret)
		authorize_url = auth_flow.start()

		print(_("1. Go to: %s") % authorize_url)
		print(_("2. Click \"Allow\" (you might have to log in first)"))
		print(_("3. Copy the authorization code."))
		code = input( _('Enter the authorization code here: ')).strip()

		oauth_result = auth_flow.finish(code)

		print(_("Token: %s") % oauth_result.access_token)
		print(_("4. This token you need enter into field \"token\" in config file"))

	def _get_list_file_in_place(self):
		result = []

		try:
			dbx = dropbox_main.Dropbox(self._config['token'])

			folder_metadata = dbx.files_list_folder(self._get_name_folder_for_save())
		except dropbox_main.exceptions.DropboxException as e:
			raise ScbErrorSlise( _("Error in 'Dropbox': %s") % e)

		for item in folder_metadata.entries:
			if not isinstance(item, dropbox_main.files.FileMetadata):
				continue
			if re.match(self.pattern_file_name, item.name) is None:
				continue
			result.append( item.name )

		return result

	def _delete_file(self, file_name:str):
		try:
			dbx = dropbox_main.Dropbox(self._config['token'])
			dbx.files_delete( os.path.join( self._get_name_folder_for_save(), file_name))
		except dropbox_main.exceptions.DropboxException as e:
			raise ScbErrorSlise( _("Error in 'Dropbox': %s") % e)

	def _get_name_folder_for_save(self) -> str:
		'''
		Возарвщает имя папки, куда нужно будет сохранять файлы
		'''
		return os.path.join( '/', self._config['name'])

	def _exist_folder_path(self, path:str, folder_path:str) -> bool:
		'''
		Проверяет существует ли папка в директории, по полному пути
		'''
		dbx = dropbox_main.Dropbox(self._config['token'])

		folder_metadata = dbx.files_list_folder(path)

		for item in folder_metadata.entries:
			if not isinstance(item, dropbox_main.files.FolderMetadata):
				continue
			if item.path_lower == folder_path.lower():
				return True

		return False
	
	def _upload_chunks(self, fils_src, dbx, fdcr, dst_path):
		"""Загружает файл кусочками, актуально для больших файлов"""
		chunk_size = 40 * 1024 * 1024  # 40Mb
		
		file_size = os.path.getsize(fils_src)
		if file_size <= chunk_size:
			return dbx.files_upload(fdcr.read(), dst_path)

		upload_session_start_result = dbx.files_upload_session_start(
			fdcr.read(chunk_size)
		)
		cursor = dropbox_main.files.UploadSessionCursor(
			session_id=upload_session_start_result.session_id,
			offset=fdcr.tell(),
		)
		commit = dropbox_main.files.CommitInfo(path=dst_path)

		while fdcr.tell() < file_size:
			if ((file_size - fdcr.tell()) <= chunk_size):
				return dbx.files_upload_session_finish(
					fdcr.read(chunk_size), cursor, commit)
			dbx.files_upload_session_append(
				fdcr.read(chunk_size),
				cursor.session_id,
				cursor.offset
			)
			cursor.offset = fdcr.tell()
