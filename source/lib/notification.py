from enum import Enum

from lib.i18n import _

class status_notify(Enum):
	good = 1
	bad = 2
	none = 0

class notification:
	'''
	Формирует сообщение об состоянии бэкапа
	'''

	msg_format = _("{slise_name} - {slise_status}\nThe target:\n{target_list}\nBackup Locations:\n{place_list}")

	msg_format_target = _("    * {name} - {status}. {status_desc}")

	def __init__(self, slise_name:str):
		'''
		slise_name - имя slise, для которого формируется отчтет
		'''
		self.slise_name = slise_name

		self.target_list = []
		self.place_list = []

		self.status = status_notify.good

	def add_target(self, name, status, status_description=''):
		self.target_list.append( {
			"name": name,
			"status": status,
			"status_description": status_description
		})
		
		if status == status_notify.bad:
			self.status = status

	def add_place(self, name, status, status_description=''):
		self.place_list.append( {
			"name": name,
			"status": status,
			"status_description": status_description
		})
		
		if status == status_notify.bad:
			self.status = status

	def get_body(self)->str:
		'''
		Получить текст сообщения
		об статусе
		'''
		target_list_msg = ""
		for item in self.target_list:
			msg_item = self.msg_format_target.format(name= item['name'], status= self._get_status_name(self.status), status_desc= item['status_description'])

			target_list_msg += msg_item
			target_list_msg += "\n"

		place_list_msg = ""
		for item in self.place_list:
			msg_item = self.msg_format_target.format(name= item['name'], status= self._get_status_name(self.status), status_desc= item['status_description'])

			place_list_msg += msg_item
			place_list_msg += "\n"

		msg = self.msg_format.format(
			slise_name= self.slise_name, 
			slise_status= self._get_status_name(self.status),
			target_list= target_list_msg,
			place_list= place_list_msg
		)


		return msg

	def _get_status_name(self, status):
		if status == status_notify.good:
			return _("Success")
		if status == status_notify.bad:
			return _("Error")

		return _("Unknown")
