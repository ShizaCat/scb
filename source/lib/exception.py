class ScbException(Exception):
	'''
	Базовый класс исключений
	'''
	def __init__(self, arg=''):
		self._arg = arg

	def __str__(self):
		return self._arg

	def __repr__(self):
		return self._arg

class ScbErrorSlise(ScbException):
	'''
	Возникает если ошибка при работе slise
	'''

class ScbConfigError(ScbException):
	'''
	При ошибке в файле настроек
	'''