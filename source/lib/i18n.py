import gettext

# Set up message catalog access
t = gettext.translation(
    'scb', 'locale',
    fallback=True,
)

_ = t.gettext