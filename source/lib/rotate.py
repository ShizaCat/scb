import jsonschema

from lib.exception import ScbException
from lib.i18n import _

class rotate:

	jsonschema = {
		"type": "object",
		"propertyes": {
			"count": {"type": "number"},
		},
		"required": ["count"]
	}

	def __init__(self, config: dict, place_obj):
		self._config = config
		self._obj = place_obj

		self._check_config()
		
	def _check_config(self):
		'''
			Проверка конфига ротации
		'''
		try:
			jsonschema.validate( self._config, self.jsonschema)
		except jsonschema.exceptions.ValidationError as e:
			raise ScbException( "Ошибка в конфигурации rotate: %s" % e.message)

	def start(self):
		'''
			Запуск самой ротации
		'''
		
		count = self._config['count']

		list_file = self._obj._get_list_file_in_place()
		count_del_file = len(list_file) - count

		if count_del_file <= 0:
			return

		list_file.sort(key=self._obj._sort_file)
		for i in range( count_del_file):
			self._obj._delete_file(list_file[i])
