'''
	Содержит перечень схем для проверки формата
'''

# Схема основного формата файла настроек в json формате
schema_base_format_config = {
	"title": "Основа файла настроек",
	"type": "object",
	"properties": {
		"name": {
			"type": "string"
		},
		"places": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"name": {"type": "string"},
					"type": {"type": "string"},
				},
				"required": ["name", "type"]
			},
		},
		"slises": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"place": {
						"type": "array",
						"items": {
							"type": "string",
						},
						"minItems": 1,
					},
					"target": {
						"type": "array",
						"items": {
							"type": "object",
							"properties": {
								"name": {"type": "string"},
								"type": {"type": "string"},
							},
							"required": ["name", "type"],
						},
					},
					"name": {
						"type": "string",
					},
					"notify": {
						"type": "array",
						"items": {
							"type": "object",
							"properties": {
								"name" : {
									"type": "string",
								},
								"status": {
									"type": "string",
									"enum": ["good", "bad"],
								},
							},
							"required": ["name"],
						},
					},
				},
				"required": ["place", "target", "name"],
			},
		},
	},
	"notices": {
		"type" : "array",
		"items" : {
			"type" : "object",
			"properties" : {
				"name": {
					"type" : "string"
				},
				"type": {
					"type": "string"
				}
			},
			"required": ["name", "type"]
		},
	},
	"required": ["places", "slises"]
}