from lib.notify.base import NotifyBase
from lib.exception import ScbException
from lib.i18n import _

import os
import tarfile
import jsonschema
from datetime import datetime
import smtplib
from email.mime.text import MIMEText

class EmailNotify(NotifyBase):

	schema = {
		"type": "object",
		"description": "Формат для email",
		"properties": {
			"subject": {
				"type": "string",
			},
			"server": {
				"type": "string",
				"pattern": "^[a-zA-Z-_0-9\.]*(:[0-9]+)?$",
				"description": "Адрес smtp сервера, фортмат host[:port]",
			},
			"to": {
				"type": "array",
				"item": {
					"type": "string",
					"description": "Адреса электронной почты, куда слать"
				}
			},
			"from": {
				"type": "string",
				"description": "От кого, он же логин для авторизации, если нужно"
			},
			"password": {
				"type": "string",
				"description": "Пароль для авторизации при необходимости"
			},
			"starttls": {
				"type": "boolean",
				"description": "Использовать Start TLS"
			},
			"ssl": {
				"type": "boolean",
				"description": "Использовать SSL"
			}
		},
		"required": ["server", "to", "from"]
	}

	name = "email"

	def __init__(self, config):
		super().__init__(config)

	def _check_field_config(self):
		try:
			jsonschema.validate( self._config, self.schema)
		except jsonschema.exceptions.ValidationError as e:
			raise ScbException( _("{cls} Configuration error: {msg}").format(cls= "EmailNotify", msg= e.message))

	def send(self, body, status):
		self._setup_email()

		email_from = self._config["from"]

		msg = MIMEText(body)
		msg['Subject'] = self._config['subject'] if 'subject' in self._config else '---= Backup info =---'
		msg['from'] = email_from

		for to in self._config['to']:
			msg['to'] = to
			self.email_server.sendmail(email_from, to, msg.as_string())

	def _setup_email(self):
		tmp = self._config["server"].split(':')
		host = tmp[0]
		port = int(tmp[1]) if len(tmp) > 1 else 25
		
		ssl = True if "ssl" in self._config and self._config["ssl"] else False
		starttls = True if "starttls" in self._config and self._config["starttls"] else False
		password = self._config["password"] if "password"in self._config else None
		email_from = self._config["from"]

		timeout = 20
		
		if ssl:
			self.email_server = smtplib.SMTP_SSL(host, port, timeout= timeout)
			if not password is None:
				self.email_server.login(email_from, password)
		else:
			self.email_server = smtplib.SMTP(host, port, timeout= timeout)
			
			if starttls:
				self.email_server.ehlo()
				self.email_server.starttls()
				self.email_server.ehlo()

			if not password is None:
				self.email_server.login(email_from, password)

