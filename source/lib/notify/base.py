'''
	Базовый класс для нотификаций
'''

import jsonschema
import logging

class NotifyBase:
	
	schema_config = {
		"type": "object",
		"properties": {
			"name": {"type": "string"},
			"type": {"type": "string"},
		},
		"required": ["name", "type"]
	}

	@classmethod
	def factory(self, config: dict):
		'''
			Пораждает нужный объект для NotifyBase
		'''
		tp = config['type']
		for cl in self.__subclasses__():
			if cl.name == tp:
				return cl(config)

		raise ScbException( _("Object 'NotifyBase' of type %s not found") % tp)

	def __init__(self, config:dict):		
		self._config = config

		# Имя плагина
		self.name = ""

		self.check_field_config()

	def check_field_config(self):
		'''
			Проверяет, что все настройки коректны
			Базовая, она запускает пользовательские проверки
		'''
		try:
			jsonschema.validate( self._config, self.schema_config)

			self._check_field_config()
		except jsonschema.exceptions.ValidationError as e:
			raise ScbException( _("Configuration error 'Notify': %s") % e.message)

	def send(self, body:str):
		'''
		Отсылает сообщение, 
		функция должна быть переопределена


		Args:
			body - тело сообщения
		'''

	def _check_field_config(self):
		'''
		Должна быть переопределена
		'''
		pass