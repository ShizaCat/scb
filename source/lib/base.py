import random
import jsonschema
import re

from lib.exception import ScbException
from lib.i18n import _
from lib.rotate import rotate

class TargetBase:
	'''
		Базовый класс для целей (target)
	'''
	jsonschema = {}

	jsonschema_base = {
		"type": "object",
		"description": "Базовая схема target",
		"properties": {
			"name": {"type": "string"},
			"type": {"type": "string"},
		},
		"required": ["name", "type"]
	}

	# Имя в файле настроек
	name = ""
	description = ""

	def __init__(self, config):
		self._config = config

	@classmethod
	def factory(self, config: dict):
		'''
			Пораждает нужный объект для target
		'''
		tp = config['type']
		for cl in self.__subclasses__():
			if cl.name == tp:
				return cl(config)

		raise ScbException( _("Object 'target' of type %s not found") % tp)

	def check_field_config(self):
		'''
		Проверка формата настроек

			
		Return:
			None - если все хорошо, или вызывает исключение


		Raises:
			ScbException - в случаи нахождения ошибки
		'''
		try:
			jsonschema.validate( self._config, self.jsonschema_base)
		except jsonschema.exceptions.ValidationError as e:
			raise ScbException( _("Configuration error 'target': %s") % e.message)

	def process(self, tmp_dir: str, dir_result: str) -> str:
		'''
		Выполняет основное действие цели


		Args:
			tmp_dir - директория для временных файлов, в нее же будет положен итоговый файл
			dir_result - директория для файла результата

			
		Return:
			Имя файла, который получается после обработки. Должен лежать в директории <dir_result>


		Raises:
			ScbException - при ошибках
		'''

class PlaceBase:
	'''
		Базовый класс для мест (Place)
	'''

	jsonschema = {
		"type": "object",
		"propertyes": {
			"name": {
				"type": "string",
			},
			"type": {"type": "string"},
			"rotate": {
				"type": "object"
			},
		},
		"required": ["name", "type"]
	}

	# Патерня для фильтрования файлов в хранилище
	pattern_file_name = ".*_\d{4}\.\d{2}\.\d{2}(\.\d{4})?\.tar"

	description = ""

	@classmethod
	def factory(self, config: dict):
		'''
			Пораждает нужный объект для place
		'''
		tp = config['type']
		for cl in self.__subclasses__():
			if cl.name == tp:
				return cl(config)

		raise ScbException( _("Object 'place' of type %s not found") % tp)

	def __init__(self, config: dict):
		'''
		Тут должна выполняться проверка конфигурации
	

		Args:
			config - словарь, настройки
		'''
		self._config = config
		self.check_field_config()

	def check_field_config(self):
		'''
			Проверяет, что все настройки коректны
			Базовая, она запускает пользовательские проверки
		'''
		try:
			jsonschema.validate( self._config, self.jsonschema)

			self._check_field_config()
		except jsonschema.exceptions.ValidationError as e:
			raise ScbException( _("Configuration error 'place': %s") % e.message)

	def process(self, result_file:str):
		'''
			Процесс отсылки, копирования, обработки файла


			Args:
			result_file - путь к результирующему файлу
		'''
		self._process(result_file)
		self._rotate()

	def _rotate(self):
		'''
			Запуск ротации файлов
		'''
		if 'rotate' in self._config:
			rt = rotate(self._config['rotate'], self)
			rt.start()

	def _need_add_random(self, file_name:str) -> str:
		'''
		Нужно ли добавлять индекс к файлу,
		существует ли такой файл уже

		
		Args:
			file_name - имя файла, которое нужно проверить


		Return:
			Возвращает имя файла, с уже добавленными индексом, если это надо, или тоже самое
		'''
		file_part_one = '.'.join(file_name.split('.')[:-1])
		file_part_two = file_name.split('.')[-1:][0]

		pattern = '^' + re.escape(file_part_one) + '\.?([0-9]{4})?' + '\.' + re.escape(file_part_two) + '$'

		for item in self._get_list_file_in_place():
			if re.match(pattern, item) is None:
				continue	
			return self._add_random_bit_to_file_name(file_name)
		
		return file_name

	def _add_random_bit_to_file_name(self, file_name:str) -> str:
		'''
			Добавляет цифровую часть к имени файла,
			на случай если такой файл уже есть в директории
			куда он копируется
			Возврастает с каждым разом на 1
			


			Args:
				file_name има файла
		'''
		file_part_one = '.'.join(file_name.split('.')[:-1])
		file_part_two = file_name.split('.')[-1:][0]

		pattern = '^' + re.escape(file_part_one) + '\.([0-9]{4})' + '\.' + re.escape(file_part_two) + '$'

		files = self._get_list_file_in_place()

		ind_list = []
		for item in files:
			gr = re.search(pattern, item)
			if not gr is None:
				ind_list.append( gr.group(1))

		ind_list.sort()
		if len(ind_list) > 0:
			try:
				index = int(ind_list[-1:][0])
			except ValueError:
				index = 0
		else:
			index = 0

		index += 1

		file_name_new = file_part_one + '.' + '%04i' % index + '.' + file_part_two
		
		return file_name_new

	def _sort_file(self, item):
		'''
		Функция для правильной сортировки файлов
		'''
		if item[-7] == ".":
			file_part_one = '.'.join(item.split('.')[:-1])
			file_part_two = item.split('.')[-1:][0]
			return file_part_one + '.0000.' + file_part_two
		return item

# ===============================

	def _check_field_config(self):
		'''
			Проверяет, что все настройки коректны
			Должна быть переопределена
		'''
		pass

	def _process(self, result_file):
		'''
			Функция, которая должна быть переопределена
			Процесс отсылки, копирования, обработки файла


			Args:
			result_file - строка, путь к результирующему файлу
		'''
		pass

	def init(self):
		'''
			Функция, которая должна быть переопределена
			Запуск инициализации для секций мест (place),
			если это необходимо,
			Возможно взаимодействие с пользователем
		'''

	def _get_list_file_in_place(self) -> list:
		'''
			Функция, которая должна быть переопределена
			Возвращает список файлов для этого места,
			упорядоченный по имени согласно функции _sort_file


			Returns:
				List [str], список имен файлов
		'''
		return []

	def _delete_file(self, file_name: str):
		'''
		Функция, которая должна быть переопределена
		Удаляет файл file_name
		'''


