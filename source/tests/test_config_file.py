'''
Тесты для правильной обработки файла настроек
'''

import unittest
import jsonschema

from lib.schema import schema_base_format_config
from lib.main import scb
from lib.exception import ScbConfigError

class test_config_file_main(unittest.TestCase):

	def setUp(self):
		pass

	def test_schema(self):
		jsonschema.Draft4Validator.check_schema(schema_base_format_config)

	def test_place_section(self):
		payload = {
			"places": [
				{ "name": "test" }
			],
			"slises": []
		}

		with self.assertRaises(ScbConfigError):
			obj = scb(payload)
			obj.check_config()

		# ----------

		payload = {
			"places": [
				{ 
					"name": "test",
					"type": "None"
				}
			],
			"slises": []
		}

		with self.assertRaises(ScbConfigError):
			obj = scb(payload)
			obj.check_config()

	def test_place_folder(self):
		payload = {
			"places": [
				{ 
					"name": "test",
					"type": "folder"
				}
			],
			"slises": []
		}

		with self.assertRaises(ScbConfigError):
			obj = scb(payload)
			obj.check_config()

		# ----

		payload = {
			"places": [
				{ 
					"name": "test",
					"type": "folder",
					"path": "test",
				}
			],
			"slises": []
		}

		with self.assertRaises(ScbConfigError):
			obj = scb(payload)
			obj.check_config()

		# ----

		payload = {
			"places": [
				{ 
					"name": "test",
					"type": "folder",
					"path": "/tmp",
				}
			],
			"slises": []
		}

		obj = scb(payload)
		obj.check_config()

	def test_place_dropbox(self):
		pass

	def test_slise_file(self):
		payload = {
			"places": [],
			"slises": [{
				"name": "test",
				"type": "file",
				"place": [],
				"target": [],
			}],
		}

		with self.assertRaises(ScbConfigError):
			obj = scb(payload)
			obj.check_config()

		# ----
		# Неизвестный тип target

		payload = {
			"places": [{"name":"test", "type":"folder", "path":"/tmp"}],
			"slises": [{
				"name": "test",
				"place": ["test"],
				"target": [
					{"name": "test", "type": "none"}
				],
			}],
		}

		with self.assertRaises(ScbConfigError):
			obj = scb(payload)
			obj.check_config()

		# ----

		payload = {
			"places": [],
			"slises": [{
				"name": "test",
				"type": "file",
				"place": ["test_place"],
				"target": [],
			}],
		}

		with self.assertRaises(ScbConfigError):
			obj = scb(payload)
			obj.check_config()


		# ----

		payload = {
			"places": [{"name" : "test_place", "type" : "folder", "path" : "/tmp"}],
			"slises": [{
				"name": "test",
				"type": "file",
				"place": ["test_place"],
				"target": [],
			}],
		}

		obj = scb(payload)
		obj.check_config()
