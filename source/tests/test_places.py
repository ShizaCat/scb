import unittest
import tempfile
import os
import re

from lib.base import PlaceBase

class TestPlaces(unittest.TestCase):

	def test_need_add_random_work(self):
		config = {
			"name": "",
			"type": "",
		}
		obj = PlaceBase(config)

		with tempfile.TemporaryDirectory() as tmpDir:
			name = "test test().tar"

			full_path = os.path.join(tmpDir, name)
			with open(full_path, "w") as f:
				f.write('test')

			# Файла такого нет
			r = obj._need_add_random(name)
			self.assertEqual(r, name)
			
			# Такой файл есть
			obj._get_list_file_in_place = lambda: [name]

			r = obj._need_add_random(name)
			self.assertEqual(r, "test test().0001.tar")

			# Увеленичение индекса
			obj._get_list_file_in_place = lambda: [name, "test test().0001.tar"]

			r = obj._need_add_random(name)
			self.assertEqual(r, "test test().0002.tar")

	def test_pattern_format(self):
		right_name = [
			"test_2017.01.01.tar",
			"test_2017.01.01.0001.tar",
			"test another.filename_2017.01.01.tar",
			"test another.filename_2017.01.01.0001.tar",
			"test another.(0001).[filename]_2017.01.01.tar",
			"test another.(0001).[filename]_2017.01.01.0001.tar",
			"test another.0002.name_2017.01.01.tar",
			"test another.0002.name_2017.01.01.0001.tar",
		]

		for name in right_name:
			r = re.match( PlaceBase.pattern_file_name, name)
			self.assertEqual(r is None, False)
		
		# -----

		not_right_name = [
			"test_2017.01.01",
			"test_2017.01.01.002.tar",
			"test_2017.01.01.aaaa.tar",
		]

		for name in not_right_name:
			r = re.match( PlaceBase.pattern_file_name, name)
			self.assertEqual(r is None, True)


