��    '      T  5   �      `     a  &   �     �     �     �  1   �       >   '      f     �      �     �  #   �                    4  #   @  "   d  '   �  %   �     �     �  (     #   -  $   Q     v     �  �  �     �     �     �  *   �  	   �     �     	      !	  V   B	  �  �	  +   7  &   c     �  #   �     �  �   �  6   ^  z   �  6     5   G  6   }  ,   �  7   �       .   &     U     q  7   �  :   �  8   �  6   .  %   e  %   �  ;   �  6   �  7   $  /   \     �  z  �  
        (  +   ?  Y   k     �     �  4   �  1     ]   Q                     '       
                             	   #                                             "          $                           %      &   !                          
The operation was interrupted     * {name} - {status}. {status_desc}     {} - {} 'Place' %s not found 1. Go to: %s 2. Click "Allow" (you might have to log in first) 3. Copy the authorization code. 4. This token you need enter into field "token" in config file Configuration error 'Notify': %s Configuration error 'place': %s Configuration error 'target': %s Configuration errors: %s Enter the authorization code here:  Error Error copying: %s Error in 'Dropbox': %s Error: 
	{} Files will be copied to the dropbox Files will be copied to the folder For notify not found notice: '{notice}' For target not found place: '{place}' Initializing for: %s Makes mysql database dump Object 'NotifyBase' of type %s not found Object 'place' of type %s not found Object 'target' of type %s not found Perform the following steps: Place plugins: Simple CLI Backup
Usage:
	scb.py --config=<config_file>
	scb.py init --config=<config_file>
	scb.py --config-test=<config_file>
	scb.py (-h | --help)
	scb.py (-v | --version)
	scb.py --plugin-info

Options:
	-h --help           Show this message
	-v --version        Show version
	--config            The path to the file with the settings
	--config-test       Testing config file
	init                Initializing modules, if necessary

	--plugin-info      Displays information about plugins
 Success Target plugins: The file of settings not found The path %s not exists or he is not folder Token: %s Unknown Working with file and folder {cls} Configuration error: {msg} {slise_name} - {slise_status}
The target:
{target_list}
Backup Locations:
{place_list} Project-Id-Version: 
POT-Creation-Date: 2017-10-12 21:55+0300
PO-Revision-Date: 2017-10-12 21:57+0300
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.0.1
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
Операция была прервана     * {name} - {status}. {status_desc}     {} - {} Не найдено ‘place’: %s 1. Перейдите по: %s 2. Нажмите «Разрешить» (возможно вам понадобиться в начале залогиниться) 3. Скопируйте код авторизации. 4. Этот токен вам нужно ввести в поле «token» в конфигурационном файле Ошибка конфигурации ‘Notify’: %s Ошибка конфигурации ‘place’: %s Ошибка конфигурации ‘target’: %s Ошибка в конфигурации: %s Введите код авторизации сюда:  Ошибка Ошибка при копировании: %s Ошибка в Dropbox: %s Ошибка
	{} Файлы будут скопированы в dropbox Файлы будут скопированы в папку Для notify не найдена notice: ‘{notice}’ Для target не найдено place: ‘{place}’ Инициализация для: %s Делает дамб базы  mysql Не найден объект ‘NotifyBase’ типа %s Не найден объект ‘place’ типа %s Не найден объект ‘target’ типа %s Выполните следующие шаги: Place плагины: Simple CLI Backup
Usage:
	scb.py --config=<config_file>
	scb.py init --config=<config_file>
	scb.py --config-test=<config_file>
	scb.py (-h | --help)
	scb.py (-v | —version)

Options:
	-h --help           Показать это сообщение
	-v —version        Показать версию
	--config            Путь к файлу с настройками
	--config-test       Проверить файл с настройками
	init                Инициализация модулей, если это необходимо

	—plugin-info      Отображает информацию об плагинах
 Успех Target плагены: Не найден файл настроек Путь %s не существует или не является директорией Токен: %s Не известно Работает с файлами и папками {cls} Ошибка в настройках: {msg} {slise_name} - {slise_status}
Цели:
{target_list}
Места бэкапов:
{place_list} 